#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
#include "MemoryAccess.h"
#include "AlbumManager.h"
#include "DatabaseAccess.h"

int getCommandNumberFromUser();
void welcome();

int main(void)
{
	// initialization data access
	DatabaseAccess dataAccess;

	// initialize album manager
	AlbumManager albumManager(dataAccess);
	

	std::string albumName;
	welcome();

	do {
		int commandNumber = getCommandNumberFromUser();
		
		try	{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		} catch (std::exception& e) {	
			std::cout << e.what() << std::endl;
		}
	} 
	while (true);
}


int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");
	
	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);
	
	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}
	
	return std::atoi(input.c_str());
}

void welcome()
{
	std::cout << "Welcome to Hillel Levy's Gallery!" << std::endl;
	std::cout << "===================================" << std::endl;
	auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::cout << std::ctime(&t) << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;

}