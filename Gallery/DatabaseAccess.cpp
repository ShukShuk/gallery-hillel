#include "DatabaseAccess.h"
#include "ItemNotFoundException.h"
#include <vector>

//int callback(void *data, int argc, char **argv, char **azColName)
//{
//
//}

int nameCallback(void *data, int argc, char **argv, char **azColName)
{
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "name")
		{
			((std::vector<std::string>*)data)->push_back(argv[i]);
		}
	}
	return 0;
}

int countCallback(void *data, int argc, char **argv, char **azColName)
{
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "count")
		{
			if (argv[i] != NULL)
			{
				*(int*)data += atoi(argv[i]);
			}
		}
	}

	return 0;
}

int listCallback(void *data, int argc, char **argv, char **azColName)
{
	Album tempAlbum;

	for (int i = 0; i < argc; i++) 
	{
		if (std::string(azColName[i]) == "name")
		{
			tempAlbum.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "creation_date")
		{
			tempAlbum.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "user_id")
		{
			tempAlbum.setOwner(atoi(argv[i]));
		}
	}
	((std::list<Album>*)data)->push_back(tempAlbum);
	
	return 0;
}

int userListCallback(void *data, int argc, char **argv, char **azColName)
{
	std::string name;
	int id;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "name")
		{
			name = argv[i];
		}
		else if (std::string(azColName[i]) == "id")
		{
			id = atoi(argv[i]);
		}
		
	}
	User tempUser(id, name);

	((std::list<User>*)data)->push_back(tempUser);

	return 0;
}

int tagCallback(void *data, int argc, char **argv, char **azColName)
{
	((Picture*)data)->tagUser(atoi(argv[0]));
	return 0;
}

int pictureCallback(void *data, int argc, char **argv, char **azColName)
{

	Picture pic(1, "");
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "name")
		{
			pic.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "creation_date")
		{
			pic.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "location")
		{
			pic.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "id")
		{
			pic.setId(atoi(argv[i]));
		}

	}
	((std::list<Picture>*)data)->push_back(pic);
	

	return 0;
}


const std::list<Album> DatabaseAccess::getAlbums()
{
	std::string sqlStatement = "select * from albums;";

	std::list<Album> albums;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), listCallback, &albums, &errMessage);

	return albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User & user)
{
	std::string sqlStatement = "select * from albums where user_id == " + std::to_string(user.getId()) + ";";

	std::list<Album> albums;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), listCallback, &albums, &errMessage);

	return albums;
}

void DatabaseAccess::createAlbum(const Album & album)
{
	std::string sqlStatement = "insert into albums (name, creation_date, user_id) values(\"" + album.getName() + "\", \"" + album.getCreationDate() + "\", " + std::to_string(album.getOwnerId()) + ");";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

}

void DatabaseAccess::deleteAlbum(const std::string & albumName, int userId)
{
	std::string sqlStatement = "select pictures.name from pictures left join albums on albums.id == album_id where albums.user_id == " + std::to_string(userId) + " and albums.name like \"" + albumName + "\";";

	char* errMessage = nullptr;
	std::vector<std::string> picture_names;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nameCallback, &picture_names, &errMessage);

	for (int i = 0; i < picture_names.size(); i++)
	{
		removePictureFromAlbumByName(albumName, picture_names[i]);
	}

	sqlStatement = "delete from albums where name like \"" + albumName + "\" and user_id == " + std::to_string(userId) + ";";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	/*if (res != SQLITE_OK)
	{
		
	}*/
}

bool DatabaseAccess::doesAlbumExists(const std::string & albumName, int userId)
{
	std::string sqlStatement = "select count(id) as count from albums as count where user_id == " + std::to_string(userId) + " and name like \"" + albumName + "\";";

	char* errMessage = nullptr;
	int album_count = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &album_count, &errMessage);

	return album_count != 0;
}

Album DatabaseAccess::openAlbum(const std::string & albumName)
{
	std::string sqlStatement = "select * from albums where name like \"" + albumName + "\";";

	std::list<Album> albums;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), listCallback, &albums, &errMessage);
	
	Album album(albums.front());
	std::list<Picture> pics;
	sqlStatement = "select pictures.* from pictures left join albums on albums.id == pictures.album_id where albums.name like \"" + albumName + "\";";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), pictureCallback, &pics, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	
	for (auto& pic : pics) 
	{
		std::string sqlStatement = "select user_id from tags where picture_id == " + std::to_string(pic.getId()) + ";";

		char* errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), tagCallback, &pic, &errMessage);
		album.addPicture(pic);
	}
	return album;
}

void DatabaseAccess::closeAlbum(Album & pAlbum)
{

}

void DatabaseAccess::printAlbums()
{
	std::list<Album> albums(getAlbums());

	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : albums) 
	{
		std::cout << std::setw(5) << "* " << album;
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string & albumName, Picture & picture)
{
	std::string sqlStatement = "select max(id) as count from pictures limit 1;";

	char* errMessage = nullptr;
	int next_id = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &next_id, &errMessage);

	picture.setId(next_id == 0 ? 101 : ++next_id);

	sqlStatement = "insert into pictures select " + std::to_string(picture.getId()) + ", \"" + picture.getName() + "\", \"" + picture.getPath() + "\", \"" + picture.getCreationDate() + "\", albums.id from albums where albums.name like \"" + albumName + "\";";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

}

void DatabaseAccess::removePictureFromAlbumByName(const std::string & albumName, const std::string & pictureName)
{
	std::string sqlStatement = "delete from tags where picture_id in (select pictures.id from pictures left join albums on pictures.album_id == albums.id where pictures.name like \"" + pictureName + "\" and albums.name like \"" + albumName + "\");" + " delete from pictures where album_id in (select albums.id from albums where albums.name like \"" + albumName + "\") and pictures.name like \"" + pictureName + "\";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

}

void DatabaseAccess::tagUserInPicture(const std::string & albumName, const std::string & pictureName, int userId)
{
	std::string sqlStatement = "insert into tags (user_id, picture_id) select " + std::to_string(userId) + ", pictures.id from pictures left join albums on albums.id == pictures.album_id where albums.name like \"" + albumName + "\" and pictures.name like \"" + pictureName + "\";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
}

void DatabaseAccess::untagUserInPicture(const std::string & albumName, const std::string & pictureName, int userId)
{
	std::string sqlStatement = "delete from tags where picture_id in (select pictures.id from pictures left join albums on albums.id == pictures.album_id where pictures.name like \"" + pictureName + "\" and albums.name like \"" + albumName + "\") and user_id == " + std::to_string(userId) + ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
}

void DatabaseAccess::printUsers()
{
	std::list<User> userList;

	std::string sqlStatement = "select * from users;";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), userListCallback, &userList, &errMessage);

	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const auto& user : userList) 
	{
		std::cout << user << std::endl;
	}
}

User DatabaseAccess::getUser(int userId)
{
	std::list<User> userList;

	
	std::string sqlStatement = "select * from users where id == " + std::to_string(userId) + ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), userListCallback, &userList, &errMessage);

	return userList.front();
}

void DatabaseAccess::createUser(User & user)
{
	std::string sqlStatement = "select max(id) as count from users limit 1;";

	char* errMessage = nullptr;
	int next_id = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &next_id, &errMessage);

	user.setId(next_id == 0 ? 201 : ++next_id);

	sqlStatement = "insert into users values (" + std::to_string(user.getId()) + ", \"" + user.getName() + "\");";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException("error creating user");
	}
}

void DatabaseAccess::deleteUser(const User & user)
{
	std::string sqlStatement = "select name from albums where user_id == " + std::to_string(user.getId()) + ";";

	char* errMessage = nullptr;
	std::vector<std::string> album_names;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nameCallback, &album_names, &errMessage);

	for (int i = 0; i < album_names.size(); i++)
	{
		deleteAlbum(album_names[i], user.getId());
	}
	
	sqlStatement = "delete from tags where user_id == " + std::to_string(user.getId()) + ";" + "delete from users where id == " + std::to_string(user.getId()) + ";";
	
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
}

bool DatabaseAccess::doesUserExists(int userId)
{
	std::string sqlStatement = "select count(id) as count from users where id == " + std::to_string(userId) + ";";

	char* errMessage = nullptr;
	int album_count = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &album_count, &errMessage);

	return album_count != 0;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User & user)
{
	std::string sqlStatement = "select count(id) as count from albums where user_id == " + std::to_string(user.getId()) + " group by id;";

	char* errMessage = nullptr;
	int album_count = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &album_count, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	return album_count;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User & user)
{
	std::string sqlStatement = "select count(albums.id) as count from albums left join pictures, tags on pictures.album_id == albums.id and tags.picture_id == pictures.id where albums.user_id == " + std::to_string(user.getId()) + " and tags.user_id == " + std::to_string(user.getId()) + ";";

	char* errMessage = nullptr;
	int album_count = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &album_count, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	return album_count;
}

int DatabaseAccess::countTagsOfUser(const User & user)
{
	std::string sqlStatement = "select count(id) as count from tags where user_id == " + std::to_string(user.getId()) + ";";

	char* errMessage = nullptr;
	int tags_count = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), countCallback, &tags_count, &errMessage);

	return tags_count;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User & user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (albumsTaggedCount == 0) 
	{
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

User DatabaseAccess::getTopTaggedUser()
{
	std::list<User> userList;

	std::string sqlStatement = "select * from users where id in (select user_id from tags group by user_id order by count(*) desc limit 1);";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), userListCallback, &userList, &errMessage);
	if (userList.size() == 0)
	{
		throw MyException("No Tags.");
	}
	return userList.front();
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	std::list<Picture> pictureList;

	std::string sqlStatement = "select * from pictures where id in (select picture_id from tags group by picture_id order by count(*) desc limit 1);";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), pictureCallback, &pictureList, &errMessage);
	if (pictureList.size() == 0)
	{
		throw MyException("No Tags.");
	}
	return pictureList.front();
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User & user)
{
	std::list<Picture> pictureList;

	std::string sqlStatement = "select * from pictures where id in (select picture_id from tags where user_id == " + std::to_string(user.getId()) + " group by picture_id order by count(*) desc);";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), pictureCallback, &pictureList, &errMessage);
	if (pictureList.size() == 0)
	{
		throw MyException("No Tags.");
	}

	return pictureList;
}

bool DatabaseAccess::open()
{
	int doesFileExist = _access(dbFileName, 0);
	int res = sqlite3_open(dbFileName, &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return -1;
	}
	if (doesFileExist == -1) 
	{
		char* sqlStatement = "create table if not exists users(id integer primary key, name text); create table if not exists albums(id integer primary key, name text, creation_date date, user_id integer, foreign key (user_id) references users(id)); create table if not exists pictures(id integer primary key, name text, location text,creation_date date, album_id integer, foreign key (album_id) references albums(id)); create table if not exists tags(id integer primary key, picture_id integer, user_id integer, foreign key (picture_id) references pictures(id), foreign key (user_id) references users(id));";
		char* errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			return false;
		}
	}
	

	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}

void DatabaseAccess::clear()
{

}
